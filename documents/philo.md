# Philosophie

# Capitalise 

- [Ayn Rand, la gourou du capitalisme](https://www.youtube.com/watch?v=xFJ0go1EOBg)
- [Comprendre la destruction créatrice [Philippe Gattet]Title](https://www.youtube.com/watch?v%253DRgtsEdozYfI)
- [Introduction à Schumpeter](https://www.cairn.info/revue-l-economie-politique-2006-1-page-82.htm)
- [Darwin contre le « Darwinisme social »](https://www.youtube.com/watch?v%253D0myevHgYIxw)
- [Darwin et Kropotkine, compétion ou solidarité ?](https://www.youtube.com/watch?v%253DSs4kNrxCllM)


## Souveraineté
* [<i class="fas fa-newspaper"></i> Code is law / le code fait loi (*Lawrence Lessig*)](https://framablog.org/2010/05/22/code-is-law-lessig/)

## Le logiciel libre

* [<i class="fas fa-book"></i> Richard Stallman et la révolution du logiciel libre - Une biographie autorisée (*Sam Williams, Richard Stallman &Christophe Masutti*)](https://framabook.org/docs/stallman/framabook6_stallman_v1_gnu-fdl.pdf)
* [<i class="fas fa-film"></i> The Internet's Own Boy - l'histoire d'Aaron Schwartz](https://www.youtube.com/watch?v=7ZBe1VFy0gc)

## Les Hackers
* [<i class="fab fa-youtube"></i> L'éthique des hackers (*Steven Levy*)](https://www.editions-globe.com/lethique-des-hackers/)


## Dystopie

- [Aldous Huxley, comment devenir soi dans un monde dystopique ?](https://www.radiofrance.fr/franceculture/podcasts/les-chemins-de-la-philosophie/les-chemins-de-la-philosophie-du-vendredi-10-juin-2022-7728911)

