# Surveillance

* [Fichage en france](https://fr.wikipedia.org/wiki/Fichage_en_France)
* [<i class="fas fa-newspaper"></i> Loi de Programmation Militaire 2014-2019 :  Eléments d’évaluation du risque législatif lié à l’article 13](http://ifrei.org/tiki-download_file.php?fileId=59)
* [Tous surveillés - 7 milliards de suspects !](https://www.arte.tv/fr/videos/083310-000-A/tous-surveilles-7-milliards-de-suspects/)
* [Rien à cacher](https://www.youtube.com/watch?v=djbwzEIv7gE)
* [Citizen four - (*Laura Poitras*)](https://www.youtube.com/watch?v=ABBPRd7ARYM)
* [Le cinquième pouvoir](https://fr.wikipedia.org/wiki/Le_Cinqui%C3%A8me_Pouvoir)
* [Underground : L'Histoire de Julian Assange](https://fr.wikipedia.org/wiki/Underground_:_L%27Histoire_de_Julian_Assange)
* [Snowden](https://fr.wikipedia.org/wiki/Snowden_(film))
* [surveillance:// (*Tristan Nitot*)](http://standblog.org/blog/pages/Surveillance)
* [Attentifs ensemble! (*Jérôme Thorel*)](http://www.editionsladecouverte.fr/catalogue/index-Attentifs_ensemble__-9782707174215.html)
* [Menaces sur nos libertés (*Julian Assange / Jacob Appelbaum / Müller-Maguhn / Jérémie Zimmermann*)](https://korben.info/menace-sur-nos-libertes-assange.html)
* [Guide d'autodéfense numérique](https://guide.boum.org/)
* [Mémoires Vives (*Edward Snowden*)](https://fr.wikipedia.org/wiki/M%C3%A9moires_vives_(livre))
* [L’Âge du capitalisme de surveillance (*Shoshana Zuboff*)](https://www.zulma.fr/livre/lage-du-capitalisme-de-surveillance/)

- « À l'ère de la surveillance numérique » 7
    - [Échapper à la surveillance](https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire/echapper-a-la-surveillance-4257864)
    - [Dans les allées de la safe city](https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire/dans-les-allees-de-la-safe-city-4917774)
    - [Géopolitique de la surveillance numérique](https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire/geopolitique-de-la-surveillance-numerique-8848321)
    - [Le capitalisme de surveillance](https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire/le-capitalisme-de-surveillance-8145806)

#### Piraterie

* [TPB AFK: The Pirate Bay Away From Keyboard](https://www.youtube.com/watch?v=eTOKXCEwo_8)

#### Sécurité

* [ANSSI](https://www.ssi.gouv.fr/)
* [Les marchands de peur / La bande à Bauer et l'idéologie sécuritaire (*Mathieu Rigouste*)](https://www.editionslibertalia.com/catalogue/a-boulets-rouges/Les-Marchands-de-peur)
* [Cybertraque](https://fr.wikipedia.org/wiki/Cybertraque_(film))


