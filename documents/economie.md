# Economie

## Le capitalisme américain

- [Capitalisme américain, le culte de la richesse (1/3)](https://www.youtube.com/watch?v=0j1UDBqR-oM&t=7s=)
- [Faire casquer les riches | Capitalisme américain, le culte de la richesse (2/3)](https://www.youtube.com/watch?v=uccQqNg2tF8&t=3s)
- [Qui veut gagner des milliards ? | Capitalisme américain, le culte de la richesse (3/3)](https://www.youtube.com/watch?v=JOQ2bj54-e0&t=4s)

* [<i class="fas fa-book"></i> Le mythe de l'entrepreneur: Défaire l'imaginaire de la Silicon Valley (*Anthony Galluzzo*)](https://www.editionsladecouverte.fr/le_mythe_de_l_entrepreneur-9782355221972)

- [Elizabeth Holmes, l’arnaqueuse de la Silicon Valley](https://www.radiofrance.fr/franceinter/podcasts/affaires-sensibles/affaires-sensibles-du-lundi-17-octobre-2022-6852465)

- [The Homestead Steel Strike](https://www.pbslearningmedia.org/resource/amex30ga-soc-homesteadstrike/the-homestead-steel-strike-the-gilded-age/)

## Entreprenariat

* [<i class="fas fa-book"></i> Steve Jobs (*Walter Isaacson*)](https://fr.wikipedia.org/wiki/Steve_Jobs_(livre))
* [<i class="fas fa-film"></i> Steve Jobs (Danny Boyle)](https://fr.wikipedia.org/wiki/Steve_Jobs_(film))
* [<i class="fas fa-film"></i> Silicon Valley (série télévisée)](https://fr.wikipedia.org/wiki/Silicon_Valley_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))

## Microtravail

* [<i class="fab fa-youtube"></i> Invisibles: Les travailleurs du clic](https://www.france.tv/slash/invisibles/saison-1/)






