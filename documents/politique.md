# Société

* [<i class="fab fa-wikipedia-w"></i> Luddisme](https://fr.wikipedia.org/wiki/Luddisme)
* [<i class="fab fa-wikipedia-w"></i> Comité pour la liquidation ou la destruction des ordinateurs (CLODO)](https://fr.wikipedia.org/wiki/Comit%C3%A9_pour_la_liquidation_ou_la_destruction_des_ordinateurs)
* [<i class="fab fa-wikipedia-w"></i> Jello Biafra](https://fr.wikipedia.org/wiki/Jello_Biafra)
* [<i class="fas fa-book"></i> Cybernétique et société (*Norbert wiener*)](http://www.seuil.com/ouvrage/cybernetique-et-societe-norbert-wiener/9782757842782)
* [<i class="fas fa-book"></i> Le cimetière de Prague (*Umbero Eco*)](https://www.grasset.fr/livres/le-cimetiere-de-prague-9782246783893)
* [<i class="fas fa-book"></i> La société du risque (*Ulrich Beck*)](https://editions.flammarion.com/Catalogue/champs-essais/philosophie/la-societe-du-risque)
* [<i class="fas fa-book"></i> Surveiller et punir (*Michel Foucault*)](https://fr.wikipedia.org/wiki/Surveiller_et_punir)
* [<i class="fas fa-book"></i> Sapiens : Une brève histoire de l'humanité (*Yuval Noah Harari*)](https://www.albin-michel.fr/ouvrages/sapiens-edition-limitee-9782226445506)


