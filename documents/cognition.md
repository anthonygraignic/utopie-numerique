# Cognitiion / Culture 

* [Le bug humain (*Sébastien Bohler*)](https://fr.wikipedia.org/wiki/Le_Bug_humain)
* [La démocratie des crédules](https://fr.wikipedia.org/wiki/La_d%C3%A9mocratie_des_cr%C3%A9dules)

## Dopamine / circuit de la récompense

## Réseaux sociaux

* [ZED - Zone d'éducation documentaire - #Happy, la dictature du bonheur sur les réseaux sociaux](https://www.dailymotion.com/video/x87u2ym)
    * "on a un algorithme surpuissant qui apprend à exploiter nos vulnérabilités"


## Neuromarketing

## Novlangue

* [Novlangue (*wikipedia*)](https://fr.wikipedia.org/wiki/Novlangue)
* [Quand la "vidéoprotection" remplace la "vidéosurveillance" (*Le monde*)](https://www.lemonde.fr/societe/article/2010/02/16/quand-la-videoprotection-remplace-la-videosurveillance_1306876_3224.html)
* [sur la manipulation des mots (*Franck Lepage*)](https://www.youtube.com/watch?v=mD-G5lHXviY)
* [Le mot qui a remplacé "Hiérarchie" (*Franck Lepage*)](https://www.youtube.com/watch?v=IUB1XsT5IQU)
* [La langue de bois décryptée avec humour ! (*Franck Lepage*)](https://www.youtube.com/watch?v=oNJo-E4MEk8)

## Harcèlement

- [GamerGate, du cyberharcèlement à la haine sexiste](https://www.radiofrance.fr/mouv/podcasts/arnaques-crimes-et-putaclic/gamergate-du-cyberharcelement-a-la-haine-sexiste-4903124)

## FakeNews