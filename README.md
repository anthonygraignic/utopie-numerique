# Utopie Numérique

## Objectif

L'idée est de rassembler des **documents** de toute nature traitant de l'informatique d'un point de vue:

- historique
- économique
- philosophique
- sociologique
- politique
- anthropolgique
- et bien sûr technique

L'objectif est de porter à l'attention d'un public avant tout francophone, un faisceau de faits ou d'anectdotes, datés et sourcés.

Il n'y a pas de "ligne" pré-établie: on ne veut pas plus montrer que l'informatique est "un outil formidable, solution à tous nos problème", que l'informatique "nous amène droit dans le mur".

Les grandes orientations, objectifs et finalités peuvent être discutés dans le channel "général": globalement tout est à discuter, afin d'arriver à un process automatique qui semble cohérent à tous.

## Process

L'idée est d'échanger sur

- le **process** lui même
- les **documents** permettant de sourcer les faits et anecdotes sus cités
- les faits et anecdotes à lister dans la **chronologie**
- les **catégories** de ces documents, pour qu'ils puissent être parcourus de manière chronologique et / ou thématique
- les **outils** permettant de stocker, de présenter, et de traiter automatiquement les documents

La finalité du processus et d'automatiser l'intégration de chaque nouveau document

## Documents

Les documents peuvent être de tout type

- livre
- page web
- vidéo
- podcast

Il doivent toujours être associés à une date ou à une intervalle de temps, et à au moins une catégories.

## Chronologie

La chronologie est l'index de parcours de tous les faits et anecdotes, et documents proposés.
Elle devrait être consultable de manière ergonomique via une interface web.
Elle devrait également être filtrable par **catégorie**.

## Catégories

Les catégories permettent de qualifier les documents, notamment des les associer à des thématiques.
Les catégories déjà présentes sont discutables, et doivent être étendues et améliorés

## Outils

En terme d'outils, à date nous avons à notre disposition:
- ce discord https://discord.gg/B93eaegt
- ce repo git https://gitlab.com/mazenovi/utopie-numerique
- le système de page gitlab pour la publication
    - des ressources supplémentaire pour un hébergement web plus conséquent peuvent être envisagées

Dans un futur proche des ressources pemettant le traitement automatique des documents (LLM, speech 2 text, etc ...) peuvent être envisagées également.