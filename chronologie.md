* 500 avant Jésus christ 
    * premiers bouliers 
        * A différents endroits du monde, sans qu'ils aient eu d'intercation entre eux
* fin XIX° c'est la révolution industriel (capitalisme en roue libre, la philantropie en bandoullière, Rockfeller, JP Morgan l'état n'est rien face à liberté d'entreprendre)
* 1890 
    * Herman Hollerith invente la crate perforée et la mécanographie pour le recensement Américain, puis d'autre pays dont la Russie l'Italie et l'Allemagne
        * homme d'affaire féroce, peu scrupuleux
        * la machine enregsitre via des perforation sur des fiches carton et permet l'analyse par (groupes de) critères des données collectées
* 1892
    * Grêves de homstead: le millardaire et philantrope carnegie est aussi briseur de grêves féroce (milice armée, puis l'armée civile)
* 1896 
    * création de The Tabuling Machine Company
* 1917 
    * l'International Business Machines Corporation plus connue sous le nom « IBM », remplace TCC, sous la direction Thomas J. Watson
* 1932 
    * New Deal : plus d'état, moins de concentration des richesses
        * National Recovery Administration : labrl américzin pour les industries
* 1934 
    * Dehomag (Deutsche Hollerith Maschinen Gesellschaft), filiale du groupe IBM, contribue par ses machines à faciliter la logistique de la « Solution finale »
* 1939 
    * Hewlett & Packard se monte dans un garage de Palo Alto, qui deviendra le mythe fondateur de la Slicon Valley
        * ils ont inventé la 1ère souris / pompé par Xerox / Pompé par Jobs
* 1942 
    * conférences Macy: Elles furent notamment à l'origine du courant cybernétique, des sciences cognitives et des sciences de l'information
        * La cybernétique est l'étude des mécanismes d'information des systèmes complexes
    * Schumpeter: Capitalisme, Socialisme et Démocratie introductioen du concept de "destruction créatrice"
* 1944 
    * John von Neumann publie la Théorie des jeux: étude des interdépendances des comportements de plusieurs individus
* 1947 
    * Présentation à ces conférences de la cybernétique par le mathématicien Norbert Wiener 
* 1948 
    * Claude Shannon publie la théorie de l'information: lié au télécom. Tout information est numérisable
* 1949 
    * Buckminster Fuller le dôme géodesic
* 1950 
    * Alan Turing publie le test de Turing: proposition de test d’intelligence artificielle fondée sur la faculté d'une machine à imiter la conversation humaine
* 1957 
    * Ayn_Rand : lagrève - Débuts de la pensée libertarienne
* 1961 
    * 1er  ordinateur IBM (unique de la série) pour l'US ARMY
* 1965 
    * Loi de Moore
        * Tous les 3 ans la capacité double, croissance exponentielle
        * évolution remarquable des capacités en 40 ans
        * l'industrie approche de plus en plus des limites physiques de la micro-électronique
        * nanotech / quantique
* 1966 
    * IBM 360 ordinateurs modulablées (toujours énorme) capacité très limitée, encombrement maximum, mais bien plus rapide pour l'homme (en millird de fois)
* 1968 
    * nécommunaliste
        * transversalité des savoirs et critiques de la hiérarchie    
        * drogue perturbateur
        * étudiant recrutés
        * influence scientifque: cybernéitque Wiener + Design Buckminster Fuller
        * socle technique, implémentable: Théorie de l'information, électronique, premiers calculateur
        * whole earth catalog (lu par jobs et tous les PDG de la silicon valley)
            * catalogue d'idées, d'échanges commerciaux, ou d'échanges d'expériences
* 1969 
    * ARPANET
        * DARPA
        * réel bsoin de la transversalité des savoir
        * infiltré par cette culture (les 2 sont en californie / recutement / air du temps)
        * système de transimission d'information  fiable, robuste et distribué c'est à dire techniquement démultipliant les chemisn possibles de l'information 
            *  lorsqu'un des centres (nœuds) est virtuellement détruit, les données empruntent d'autres chemins et d'autres nœuds pour atteindre les destinataires désignés
        * on est à l'heure de l'avènement cybernétique, à l'utopie numérique
        * les nécommunalistes explosent en échec social ou matériel, quand ce n'est pas gouroutisé par un psychopathe. Ils viennent coloniser l'internet pour y faire société en communauté virtuel, avec les premiers échanges de type forum (échange avec des groupes) ou chat (une seule personne)
        * cette communauté grandit au sein du grand public        
        * à noter que l'infrastructure (ordianteurs et cable de connexion) sont financés alors par l'état et 
        * ARPANET a été écrit par le monde universitaire (c'est un projet recherhe) et non militaire, ce qui a probablement influencé l'internet que l'on connait aujourd'hui
* 1972 
    * réseau Cyclade porté par Louis Pouzin inventeur du datagrame (VS circuit virtuel)
* 1975 
    * Altair 8800 par MITS / Home Brew computer club
    * création de Microsoft par Bill Gates et Paul Allen
* 1976 
    * Milton Friedman est prix Nobel
    * Apple Computer Company par Steve Jobs et Steve Wozniak
        *  Apple I premier ordinateur personnel vendu 
    * Bill Gates et Paul Allen écrivent une lettre au home brew computer club, s'inquiétant des modes de diffusion des codes sources échappeemnt au paiement
    * une version du langage de programmation BASIC pour cet ordinateur, et ils avaient l'intention de le commercialiser
* 1977 
    * HP & Texas Instrument = calculette électronique
    * Apple II écran couleur (3 ou 4)
* 1981 
    * élection de Ronald Reagan (plus de capitalisme, moins d'état, plus d'inégalité, de pauvreté, et d'accaparement des des richesses)
    * IBM lance le Personal Computers PC et laisse la concurrence copier (le PC compatible)
        * Microsft équipe les 1er ordinateurs personneld'IBM et pirate le monde du PC en devenant l'OS par défaut
* 1983 
    * Steve Jobs débauche de chez Pepsi, John Sculley en lui demandant « Vous comptez vendre de l'eau sucrée toute votre vie ou vous voulez changer le monde avec moi ? ».
* 1984 
    * Apple lance le Macintosh dans une pub diffusée pendant la retransmission du superbowl 
        * "Pourquoi 1984 ne sera pas comme 1984"
        * et c'est trop tard :/
    * Microsoft lance Windows 1.0
* 1985 
    * Jobs se fait virer d'Apple par le PDG qu'il a choisi
* 1988 
    * création du web Par Tim Berners Lee. Pensez comme une interconnexion de savoir à la wikipedia
        * Hyper Média / parcours discursif et liens hypertextes
* 1989 
    * GNU General Public License, Richard Stallman, pose les bases du logiciel libre
* 1990 
    * création de l'electronic Frontier Foundation (EFF) par entre autre John Perry Barlow
* 1991 
    * Philip Zimmermann publie PGP Pretty Good Privacy sur le web comme logiciel libre. Se fait poursuivre pendant 3 ans, exportation de logiciels de cryptographie = secret défense en Fr
* 1994 
    * Le premier navigateur "grand public": Netscape - cookie - e-commerce et libération de la crypto
    * Le chiffre de César: utilisation des maths de manière créative 
* 1994 
    * amazon se créé: un site de vente de livres
* 1995 
    * on est quelques uns à avoir un modem 28K/56K pour se connecter à Internet
* 1996 
    * Internet est utilisé massivement aux US
        * Déclaration d'indépendance du cyberespace par John Perry Barlow, Parolier du Great Full Dead
* 1998 
    * Création de Google le moteurs de recherche efficace léger et sans pub, et son classement à la notoriété des liens (page rank)
* 2000 
    * 1ère Bulle Internet. Les business angel se retirent, les start ups sans buesiness model dévissent
* 2001 
    * le sruplus comporetemental & le patriot act
        * Eric schmidt Chief Executive Officer (CEO) de google "building the corporate infrastructure needed to maintain Google's rapid growth as a company and on ensuring that quality remains high while the product development cycle times are kept to a minimum."
    * 11/09/2001
        * tout devient business l'armée et les universités, les particuliers on un accès massif à l'internet téléphone te nouvellement ADSL
    * création de paypal
* 2006 
    * les réseau sociaux et le web 2.O
        * league du lol IUT SRC 2005 dernières explorations et  deriners fond perdu ... la fin d'une époque un changement de modèle
        * langage de programation 2 structures  logiques  if then else et while, pour controler l'information, on crée un rétroéction infie avec le like et la dopamine système à fort
        * striatum / biais cognitif
        * tout est argent
        * youtube
        * lancement d'AWS cloud
        * le volume de données explose et l'IA prend un nouveau tournant
* 2007 
    * Wikileaks 
        * the guardian / the new york times / der spiegel
* 2008 
    * les cryptmonnaies & l'uberisation
        * apparition de weakileaks
* 2010 
    * Les US autorise les entreprise à financer les campagnes des politiques (retour à la case révolution indistrielle - capitalisme dérbidé - la philantropie ne bandoullière)
* 2011 
    * mort de Steve Jobs
* 2013 whistle blowers (Snowden) & pirates (pas hacker)
    * ssi PSSI 2014
    * google glass / cyborg
* 2014 
    * google car
* 2015 
    * création d'alphabet
* 2017 
    * Neuralink
* 2020 
    * NFT (non fongible token)
        * march de l'art

* Elon Musk / Paypal Mafia

* approche extractiviste des données
    * privatisation de l'espace public avec Google maps et ses google cars
    * recherche de nouveau gisements IoT
    * hégémonie de Google dans tous les domaines santé, robotique, et bien sur informatique

* un point sur la chine et la russie
    * historique géopolitique notamment des cables sous marinset de l'isolation historique
        * souveraineté
